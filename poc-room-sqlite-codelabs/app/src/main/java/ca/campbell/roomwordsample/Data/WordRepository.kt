package ca.campbell.roomwordsample.Data

import android.app.Application
import android.arch.lifecycle.LiveData
import android.os.AsyncTask

// create a public class called WordRepository.
class WordRepository
// Add a constructor that gets a handle to the database and initializes the member variables.

(application: Application) {
    //Add member variables for the DAO and the list of words.
    private val mWordDao: WordDAO
    // Add a wrapper for getAllWords(). Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allWords: LiveData<List<Word>>

    init {
        val db = WordRoomDatabase.getDatabase(application)
        mWordDao = db!!.wordDao()
        allWords = mWordDao.allWords
    }

    fun insert(word: Word) {
        insertAsyncTask(mWordDao).execute(word)
    }

    private class insertAsyncTask internal constructor(private val mAsyncTaskDao: WordDAO) : AsyncTask<Word, Void, Void>() {

        override fun doInBackground(vararg params: Word): Void? {
            mAsyncTaskDao.insert(params[0])
            return null
        }
    }

}
