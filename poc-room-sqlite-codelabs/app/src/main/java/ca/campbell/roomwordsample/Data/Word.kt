package ca.campbell.roomwordsample.Data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "word_table")
class Word(// autoGenerate optional
        // must be int, Integer, long or Long
        // @PrimaryKey(autoGenerate = true)
        @field:PrimaryKey
        @field:ColumnInfo(name = "word")
        val word: String)
