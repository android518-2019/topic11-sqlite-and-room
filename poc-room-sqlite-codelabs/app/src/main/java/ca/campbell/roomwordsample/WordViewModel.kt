package ca.campbell.roomwordsample

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData

import ca.campbell.roomwordsample.Data.Word
import ca.campbell.roomwordsample.Data.WordRepository

class WordViewModel// Add a constructor that gets a reference to the repository and gets the list of words from the repository.
(application: Application) : AndroidViewModel(application) {
    // Add a private member variable to hold a reference to the repository.
    private val mRepository: WordRepository
    // Add a private LiveData member variable to cache the list of words.
    // Add a "getter" method for all the words. This completely hides the implementation from the UI.
    internal val allWords: LiveData<List<Word>>

    init {
        mRepository = WordRepository(application)
        allWords = mRepository.allWords
    }

    // Create a wrapper insert() method that calls the Repository's insert() method. In this way, the implementation of insert() is completely hidden from the UI.
    fun insert(word: Word) {
        mRepository.insert(word)
    }

}
