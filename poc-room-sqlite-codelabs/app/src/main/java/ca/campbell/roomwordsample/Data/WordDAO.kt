package ca.campbell.roomwordsample.Data


import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface WordDAO {
    @get:Query("select * from word_table order by word asc")
    val allWords: LiveData<List<Word>>

    @Insert
    fun insert(word: Word)

    @Query("delete from word_table")
    fun deleteAll()
}
